const path = require('path');
const bundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const webPackConfig = {
  target: "web",

  entry: {
    app: ["./app/App.tsx"]
  },

  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/build/",
    filename: "bundle.js"
  },

  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"]
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader"
      },
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      }
    ]
  },

  /*plugins: [
    new bundleAnalyzer()
  ],*/

  devtool: 'inline-source-map'
};

module.exports = webPackConfig;