import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/lab/Slider';
import {Component} from "react";

const styles = {
  root: {
    width: 300,
  },
  slider: {
    padding: '22px 0px',
  },
};

class StepSlider extends Component<any, any> {
  constructor(props) {
    super(props);
  }


  private handleChange(value) {
    this.setState({ value });
    this.props.onSliderChange(this.props.values[value]);
  };

  render() {
    return (
      <div className={'year-slider'}>
        <Slider
          classes={{
            track: 'slider-track',
            thumb: 'slider-thumb'
          }}
          value={this.props.value}
          min={0}
          max={this.props.values.length - 1}
          step={1}
          onChange={(mouseEvent, value) => this.handleChange(value)}
        />
      </div>
    );
  }
}

export default withStyles(styles)(StepSlider);