import * as React from "react";
import { Component } from "react";
import axios, {AxiosResponse} from 'axios';

import '../style.css'

import {DataSetQuery} from "../data-model/stat-fin.interface";
import {AppProps} from "../data-model/props.interface";
import {MapComponent} from "./MapComponent";

import * as RegionsOfFinland from '../static/maakuntarajat-2018.json';
import * as GDPDataSet from '../static/finnish_gdp_dataset.json';
const BigDataLogo = require('../static/big_data_logo.png')

import {DropDownQuerySelectorComponent} from "./DropDownQuerySelectorComponent";
import {DropDownStringSelector} from "./DropDownStringSelector";
import DataParseComponent, {ParsedData} from "./DataParseComponent";
import StepSlider from "./StepSlider";

export class DataSetComponent extends Component<AppProps, any> {
  private filterContainerRef: Element;

  private migrationDataTypes: {key: string, description: string}[] = [
    {key: "arrival", description: "Arrival"},
    {key: "departure", description: "Departure"}
  ];

  private GDPDataTypes: {key: string, description: string}[] = [
    {key: "totalPaidTaxes", description: "Paid Taxes"},
    {key: "taxReturns", description: "Tax Returns"},
    {key: "backPaidTaxes", description: "Back taxes"},
    {key: "totalOrganizations", description: "Amount of organizations"}
  ];

  private autoUpdateInterval: any;

  constructor(props: AppProps) {
    super(props);

    this.state = {
      currentDataSetLink: "StatFin/statfin_muutl_pxt_11a6.px",
      dataSetMetadata: null,
      dataSetResults: null,
      showFilters: true,

      allowedDisplayTypes: ["lines", "heat-map"],
      selectedDataType: null,
      displayDataType: "lines",
      selectedFilters: null,

      selectedYear: null,
      allowedYears: null
    };
  }

  componentWillMount(): void {
    document.addEventListener('mousedown', this.handleClick.bind(this), false);
  }

  componentWillUnmount(): void {
    document.removeEventListener('mousedown', this.handleClick.bind(this), false);
  }

  private handleClick(event) {
    // Hide filters on click outside
    if (this.state.showFilters && !this.filterContainerRef.contains(event.target)) {
      this.setState({
        showFilters: false
      });
    }
  }

  componentDidMount() {
    this.queryMetadata(this.state.currentDataSetLink);
  }

  /*private updateCurrentLink(newLink: string) {
    this.setState({
      currentDataSetLink: newLink
    });
  }*/

  private toggleShowFilters(): void {
    this.setState({
      showFilters: !this.state.showFilters
    })
  }

  queryDataSet(link: string, query: DataSetQuery) {
    if (!link || !query) {
      return;
    }

    const allYears: string[] = this.state.dataSetMetadata.variables.find(variable => {
      return variable.code === "Vuosi";
    }).values;

    // Query all years on default
    query.query.push({
      code: "Vuosi",
      selection: {
        filter: "item",
        values: allYears
      }
    });

    const allRegions: string[] = this.state.dataSetMetadata.variables.find(variable => {
      return variable.code === "Tulomaakunta";
    }).values.filter((value => {
      return value !== 'SSS';
    }));

    const regionCode: string = this.state.selectedDataType === 'arrival' ? "Lähtömaakunta" : "Tulomaakunta";
    const existingIndex: number = query.query.findIndex(value => {
      return value.code === regionCode;
    });

    if (existingIndex !== -1) {
      query.query.splice(existingIndex, 1);
    }

    // Query all regions
    query.query.push({
      code: this.state.selectedDataType === 'arrival' ? "Lähtömaakunta" : "Tulomaakunta",
      selection: {
        filter: "item",
        values: allRegions
      }
    });

    console.log("Query StatFin", query);

    axios.post('http://pxnet2.stat.fi/PXWeb/api/v1/en/' + link, JSON.stringify(query))
      .then((response: AxiosResponse<any>) => {
        if (response.status !== 200) {
          console.error("Failed getting dataSet", query, response.status, response);
          return;
        }

        console.log("Got dataSet from API", query, response.data);

        this.setState({
          dataSetResults: response.data,
          selectedYear: allYears[0],
          allowedYears: allYears
        })
      });
  }

  queryMetadata(queryString: string): void {
    axios.get('http://pxnet2.stat.fi/PXWeb/api/v1/en/' + queryString)
      .then((response: AxiosResponse<any>) => {
        if (response.status !== 200) {
          console.error("Failed getting metadata", queryString, response.status, response);
          return;
        }

        console.log("Got metadata from API", queryString, response.data);

        this.setState({
          dataSetMetadata: response.data
        })
      });
  }

  public setData(data: any): void {
    this.setState({
      parsedData: data
    });
  }

  private getDataTypeName(typeKey: string): string {
    if (typeKey === 'arrival' || typeKey === 'departure') {
      const typeValue: any = this.migrationDataTypes.find(type => {
        return type.key === typeKey;
      });

      return typeValue ? typeValue.description : typeKey;
    }

    const typeValue: any = this.GDPDataTypes.find(type => {
      return type.key === typeKey;
    });

    return typeValue ? typeValue.description : typeKey;
  }

  private toggleAutoUpdate(): void {
    if (this.autoUpdateInterval) {
      clearInterval(this.autoUpdateInterval);
      this.autoUpdateInterval = null;
      return;
    }

    this.autoUpdateInterval = setInterval(() => {
      const nextYearIndex: number = this.state.allowedYears.findIndex(year => {
        return year === this.state.selectedYear;
      }) + 1;

      this.setState({
        selectedYear: (nextYearIndex === this.state.allowedYears.length) ? this.state.allowedYears[0] : this.state.allowedYears[nextYearIndex]
      });
    }, 1000);
  }

  render() {
    return (
      <div className={'app-container'}>
        <img className={'logo-image'} src={BigDataLogo} alt={"BIG DATA"} />

        <div
          className={'filter-input-container ' + (this.state.showFilters ? '' : 'inputs-hidden')}
          ref={(element) => this.filterContainerRef = element}
        >
          {/*<h1>
            Insert data set link:
          </h1>

          <div>Currently only the default data set can be displayed</div>
          <input
            disabled
            className={'btn text-input'}
            value={this.state.currentDataSetLink}
            onChange={(event) => this.updateCurrentLink(event.target.value)}
          />

          <button
            className={'stylized-button'}
            onClick={() => this.queryMetadata(this.state.currentDataSetLink)}
          >
            Update metadata
          </button>*/}


          <h1>
            Migration
          </h1>

          <div className={'flex-buttons'}>
            {
              this.migrationDataTypes.map((type, index) => {
                return (
                  <button
                    className={'stylized-button'}
                    key={'data_type_migration_' + index}
                    onClick={() => this.setState({selectedDataType: type.key, dataSetResults: null})}
                  >
                    {type.description}
                  </button>
                )
              })
            }
          </div>


          <h1>
            GDP
          </h1>

          <div className={'flex-buttons'}>
            {
              this.GDPDataTypes.map((type, index) => {
                return (
                  <button
                    className={'stylized-button'}
                    key={'data_type_GDP_' + index}
                    onClick={() => this.setState({selectedDataType: type.key, displayDataType: "heat-map", selectedYear: "2012", allowedYears: ["2012", "2013", "2014", "2015", "2016", "2017"]})}
                  >
                    {type.description}
                  </button>
                )
              })
            }
          </div>

          {
            this.state.selectedDataType ?
              (
                <div>
                  <h2>
                    Filters:
                  </h2>

                  {
                    !!this.GDPDataTypes.find(type => type.key === this.state.selectedDataType) ?
                      <DropDownQuerySelectorComponent
                        dataSetMetadata={{variables: [{code: "Vuosi", text: "Year", values: Object.keys(GDPDataSet), valueTexts: Object.keys(GDPDataSet)}]}}
                        dataType={this.state.selectedDataType}
                        onConfirmQuery={(newQuery) => this.setState({selectedFilters: {years: newQuery.query[0].selection.values}})}
                      /> : null
                  }

                  {
                    !!this.migrationDataTypes.find(type => type.key === this.state.selectedDataType) ?
                      <div>
                        <DropDownQuerySelectorComponent
                          dataSetMetadata={this.state.dataSetMetadata}
                          dataType={this.state.selectedDataType}
                          onConfirmQuery={(newQuery) => this.queryDataSet(this.state.currentDataSetLink, newQuery)}
                        />

                        <DropDownStringSelector
                          currentValue={this.state.displayDataType}
                          allowedValues={this.state.allowedDisplayTypes}
                          onValueChange={(newType) => this.setState({displayDataType: newType})}
                        />
                      </div> : null
                  }
                </div>
              ) : null
          }

          <button
            className={'toggle-show-button'}
            onClick={() => this.toggleShowFilters()}
          >
            <span className={'glyphicon ' + (this.state.showFilters ? 'glyphicon-chevron-up' : 'glyphicon-chevron-down')} />
          </button>

        </div>

        <MapComponent
          geoJson={RegionsOfFinland}
          dataSet={this.state.parsedData}
          dataSetMetadata={this.state.dataSetMetadata}
          displayDataType={this.state.displayDataType}
        />

        <DataParseComponent
          rawData={(this.state.selectedDataType && !!this.migrationDataTypes.find(type => type.key === this.state.selectedDataType)) ? this.state.dataSetResults : GDPDataSet}
          dataType={this.state.selectedDataType}
          filters={this.state.selectedFilters}
          selectedYear={this.state.selectedYear}
          onParseComplete={(parsedData: ParsedData) => this.setData(parsedData)}
        />

        {
          this.state.allowedYears ?
            <div>
              <h1 className={'year-name'}>
                {this.getDataTypeName(this.state.selectedDataType) + " - " + this.state.selectedYear}
              </h1>
              <StepSlider values={this.state.allowedYears} value={this.state.allowedYears.indexOf(this.state.selectedYear)} onSliderChange={(value) => {this.setState({selectedYear: value})}} />

              <div className={'auto-update-year-button'}>
                <button
                  onClick={() => this.toggleAutoUpdate()}
                >
                  Animate year
                </button>
              </div>
            </div> : null
        }
      </div>
    )
  }
}

export default DataSetComponent
