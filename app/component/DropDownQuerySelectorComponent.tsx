import {Component} from "react";
import * as React from "react";
import {ButtonToolbar, DropdownButton, MenuItem} from 'react-bootstrap';
import {TableMetadata} from "../data-model/stat-fin.interface";
import {RegionTranslation} from "../data-model/region-translation.class";
const _ = require("lodash");

export class DropDownQuerySelectorComponent extends Component<any, any> {
  private fetchButton: Element;
  private baseState;

  constructor(props: any) {
    super(props);

    // Should only contain query parameters!
    this.state = {};
  }

  private selectDropDown(value: string, variableName: string, valueArray?: string[]): void {
    if (valueArray) {
      // Select all
      this.setState({
        [variableName]: valueArray
      });

      return;
    }

    if (!value) {
      // De-select
      this.setState({
        [variableName]: []
      });

      return;
    }

    if (!this.state[variableName]) {
      // Just add the new selection
      this.setState({
        [variableName]: [value]
      });

      return;
    }

    const updatedSelections: string[] = this.state[variableName].slice(0);
    const valueIndex: number = updatedSelections.indexOf(value);

    if (valueIndex === -1) {
      // Add selection
      updatedSelections.push(value);
    }

    else {
      // Remove selection
      updatedSelections.splice(valueIndex, 1);
    }

    this.setState({
      [variableName]: updatedSelections
    });
  }

  private createQueryFilters(): void {
    // Disable button to prevent multiple calls on button spam
    this.fetchButton.setAttribute("disabled", "disabled");

    const queryParams = Object.keys(this.state)
      .map((key) => {
        return {
          code: key,
          selection: {
            filter: "item",
            values: this.state[key] || []
          }
        };
      });

    const query = {
      query: queryParams,
      response: {
        format: 'json'
      }
    };

    // Send created query to parent
    this.props.onConfirmQuery(query);
    this.fetchButton.removeAttribute("disabled");
  }

  getRegionNameFromCode(regionCode: string): string {
    if (!regionCode) {
      return "";
    }

    const translation: RegionTranslation = RegionTranslation.GetRegionTranslation(regionCode);
    return translation ? translation.finnish : regionCode;
  }

  render() {
    return (
      <div className={'dropdown-container'}>
        <ButtonToolbar>
          {
            this.props.dataSetMetadata ?
              this.props.dataSetMetadata.variables
                .map((variable) => {
                  if (variable.code === "Vuosi") {
                    // Select all years on default
                    return null;
                  }

                  if (this.props.dataType === "arrival" && variable.code === "Lähtömaakunta") {
                    return null;
                  }

                  if (this.props.dataType === "departure" && variable.code === "Tulomaakunta") {
                    return null;
                  }

                  return (
                    <div
                      className={'dropdown-selection-container'}
                      key={variable.code + "_dropdown_container"}
                    >
                      <DropdownButton
                        bsStyle={'default'}
                        title={variable.text}
                        key={variable.code + "_dropdown"}
                        id={variable.text}
                      >
                        {/*<MenuItem
                          key={variable.code + "_select_none"}
                            eventKey={null}
                            onSelect={() => this.selectDropDown(null, variable.code)}
                          >
                          Un-select
                        </MenuItem>

                        <MenuItem
                          key={variable.code + "_select_all"}
                          eventKey={null}
                          onSelect={() => this.selectDropDown(null, variable.code, variable.values.filter((value) => value !== 'SSS'))}
                        >
                          Select All
                        </MenuItem>*/}

                        {
                          variable.values.map((value, j) => {
                            return value !== 'SSS' ? (
                              <MenuItem
                                key={variable.code + "_select_" + value}
                                eventKey={value}
                                onSelect={() => this.selectDropDown(value, variable.code)}
                              >
                                {variable.valueTexts[j]}
                              </MenuItem>
                            ) : null;
                          })
                        }
                      </DropdownButton>

                      {
                        this.state[variable.code] ?
                          <div
                            className={'selections'}
                            key={variable.code + "_unselect_buttons"}
                          >
                            {
                              this.state[variable.code].map((selection) => {
                                return (
                                  <button
                                    key={variable.code + "_unselect_" + selection}
                                    onClick={() => this.selectDropDown(selection, variable.code)}
                                  >
                                    {this.getRegionNameFromCode(selection)}
                                  </button>
                                );
                              })
                            }
                          </div>
                          : null
                      }
                    </div>
                );
                })
              : null
          }
        </ButtonToolbar>

        {
          ['arrival', 'departure'].indexOf(this.props.dataType) !== -1 ? <button
            ref={buttonElement => this.fetchButton = buttonElement}
            className={'stylized-button'}
            onClick={() => this.createQueryFilters()}
          >
            Fetch data from StatFin
          </button> :
            <div>No filters available</div>
        }
    </div>
    )
  }
}