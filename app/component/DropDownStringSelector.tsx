import {Component} from "react";
import * as React from "react";
import {ButtonToolbar, DropdownButton, MenuItem} from 'react-bootstrap';

export class DropDownStringSelector extends Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  private selectDropdown(newValue: string): void {
    this.props.onValueChange(newValue);
  }

  render() {
    return (
      <div className={'dropdown-container'}>

        <ButtonToolbar>
          <DropdownButton
            bsStyle={'default'}
            title={"Selected: " + this.props.currentValue}
            key={"string_select_dropdown"}
            id={"string_select_dropdown"}
          >
            {
              this.props.allowedValues ?
                this.props.allowedValues
                  .map((option) => {
                    return (
                      <MenuItem
                        key={option + "_select"}
                        eventKey={null}
                        onSelect={() => this.selectDropdown(option)}
                      >
                        {option}
                      </MenuItem>
                    )
                  })
                : null
            }
          </DropdownButton>
        </ButtonToolbar>
      </div>
    )
  }
}