import * as React from "react";
import { Component } from "react";
import * as ReactTooltip from 'react-tooltip';
const _ = require("lodash");

import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Markers,
  Marker,
  Lines,
  Line
} from "react-simple-maps";
import {RegionTranslation} from "../data-model/region-translation.class";
import {ParsedData} from "./DataParseComponent";

export class MapComponent extends Component<any, any> {
  private maxMarkerSize: number = 2;
  private defaultGeographyStyle: any = {
    default: {
      fill: "#545454",
      outline: "none"
    },
    hover: {
      fill: "#6c6c6c",
      outline: "none"
    },
    pressed: {
      fill: "#8c8c8c",
      outline: "none"
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      currentMarker: null,
      connectingMarkers: [],
      allMarkers: [],
      mapZoom: 5,
      geographyStyle: {}
    };
  }

  componentDidUpdate(prevProps) {
    if (!this.props.dataSet || !this.props.geoJson) {
      return;
    }

    if (prevProps.dataSet && _.isEqual(this.props.dataSet, prevProps.dataSet)) {
      // Same data as previous
      return;
    }

    this.createMarkers();
  }

  private getGeographyStyle(geography): any {
    if (!this.props.dataSet || !this.props.displayDataType || this.props.displayDataType === "lines") {
      // Show default colors
      return this.defaultGeographyStyle;
    }

    const matchingData: ParsedData = this.props.dataSet.data.find(data => {
      if (this.props.dataSet.targetRegion && this.props.dataSet.targetRegion.english === data.region.english) {
        return false;
      }

      return data.region.finnish === geography.properties.NAMEFIN;
    });

    const colorAlpha: number = matchingData ? (matchingData.value / this.props.dataSet.maxValue) + 0.1 : 0;
    const hoverAlpha: number = colorAlpha < 0.6 ? colorAlpha + 0.4 : 1;

    // Add cyan fill with alpha
    return {
      default: {
        fill: !matchingData ? "cyan" : ("rgba(0,139,139, " + colorAlpha + ")"),
        outline: "none",
        transition: "fill 0.2s linear"
      },
      hover: {
        stroke: ("rgba(0,139,139, " + hoverAlpha + ")"),
        strokeWidth: '0.1',
        strokeOpacity: '0.7',
        fill: !matchingData ? "cyan" : ("rgba(0,139,139, " + colorAlpha + ")"),
        outline: "none",
        transition: "fill 0.2s linear"
      },
      pressed: {
        fill: !matchingData ? "cyan" : ("rgba(0,139,139, " + colorAlpha + ")"),
        outline: "none"
      }
    };
  }

  private handleScroll(event) {
    // Zoom on mouse scroll
    this.setZoom(event.nativeEvent.wheelDelta > 0 ? 1 : -1);
  };

  private setZoom(changeAmount: number) {
    // Update zoom with given amount
    const newZoom: number = this.state.mapZoom + changeAmount;

    this.setState({
      mapZoom: newZoom > 0 ? newZoom : 1
    });
  };

  public static getCenterCoordinates(coordinates: [number, number][]): [number, number] {
    if (coordinates.length === 1) {
      // Some cities are broken
      coordinates = coordinates[0] as unknown as [number, number][];
    }

    // Calculate center point of given coordinates
    const xCoordinates: number[] = coordinates.map(coordinate => coordinate[0]);
    const yCoordinates: number[] = coordinates.map(coordinate => coordinate[1]);

    const xMin: number = Math.min.apply(null, xCoordinates);
    const xMax: number = Math.max.apply(null, xCoordinates);

    const yMin: number = Math.min.apply(null, yCoordinates);
    const yMax: number = Math.max.apply(null, yCoordinates);

    return [
      xMin + ((xMax - xMin) / 2),
      yMin + ((yMax - yMin) / 2)
    ];
  }

  /*public getGeographyFromDataSet(dataSet: StatFinDataSet): void {
    const yearIndex: number = dataSet.columns.findIndex((column) => column.code === "Vuosi");
    const regionOfArrivalIndex: number = dataSet.columns.findIndex((column) => column.code === "Tulomaakunta");
    const regionOfDepartureIndex: number = dataSet.columns.findIndex((column) => column.code === "Lähtömaakunta");

    const largestValue: number = Math.max.apply(Math, dataSet.data.map((result) => parseInt(result.values[0])));
    const totalValue: number = dataSet.data.map((result) => parseInt(result.values[0])).reduce((acc, a) => acc + a);

    // Parse number data from given dataSet
    const parsedLineData = dataSet.data.map((result) => {
      const year: string = yearIndex !== -1 ? result.key[yearIndex] : "?";
      const regionOfArrival: string = regionOfArrivalIndex !== -1 ? result.key[regionOfArrivalIndex] : "?";
      const regionOfDeparture: string = regionOfDepartureIndex !== -1 ? result.key[regionOfDepartureIndex] : "?";

      return {
        year: year,
        regionOfArrival: this.getRegionNameFromCode(regionOfArrival),
        regionOfDeparture: this.getRegionNameFromCode(regionOfDeparture),
        value: parseInt(result.values[0]),
        maxValue: largestValue,
        totalValue: totalValue
      }
    });

    this.setState({
      parsedLineData: parsedLineData
    });
  }*/

  createMarkers(): void {
    const connectingMarkers: any[]= [];

    // Create connecting markers to create connections between each geography
    for (const otherGeography of this.props.geoJson.features) {
      const otherCoordinates: [number, number][] = otherGeography.geometry.coordinates[0];

      connectingMarkers.push({
        markerOffset: -7,
        name: otherGeography.properties.NAMEFIN,
        coordinates: MapComponent.getCenterCoordinates(otherCoordinates)
      });
    }

    this.setState({
      allMarkers: connectingMarkers
    });
  }

  // Some regions in StatFin data have invalid codes; some in Finnish and some in English
  /*getRegionNameFromCode(regionCode: string): string {
    if (!regionCode) {
      return "";
    }

    if (!this.props.dataSetMetadata || !this.props.dataSetMetadata.variables.length) {
      return regionCode;
    }

    const regionMetaData: TableMetadata = this.props.dataSetMetadata.variables.find((metaData: TableMetadata) => {
      return metaData.code === "Tulomaakunta" || metaData.code === "Lähtömaakunta";
    });

    if (!regionMetaData) {
      return regionCode;
    }

    const regionCodeIndex: number = regionMetaData.values.indexOf(regionCode);
    const valueText: string = regionMetaData.valueTexts[regionCodeIndex];

    if (!valueText) {
      return regionCode;
    }

    let regionName: string = valueText.substr(valueText.indexOf(regionCode) + 5);
    const translation: RegionTranslation = RegionTranslation.GetRegionTranslation(regionName);

    return translation ? translation.finnish : regionName;
  }*/

  getMarkerCoordinates(cityName: string): [number, number] {
    if (!this.state.allMarkers) {
      return [0, 0];
    }

    const markerMatch = this.state.allMarkers.find((marker) => {
      return marker.name === cityName;
    });

    if (!markerMatch) {
      return [0, 0];
    }

    return markerMatch.coordinates;
  }

  private separateThousands(number: number): string {
    const splitNumber: string[] = number.toString().split(".");
    splitNumber[0] = splitNumber[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return splitNumber.join(".");
  }

  public getTooltipContent(regionName: string): any {
    if (!this.props.dataSet || !regionName) {
      return <div>
        <p className={'header'}>{regionName}</p>
      </div>;
    }

    const matchingData: ParsedData = this.props.dataSet.data.find(data => {
      if (this.props.dataSet.targetRegion && this.props.dataSet.targetRegion.english === data.region.english) {
        return false;
      }

      return data.region.finnish === regionName;
    });

    if (!matchingData) {
      return <div>
        <p className={'header'}>{RegionTranslation.GetRegionTranslation(regionName).english}</p>
      </div>;
    }

    const percentageOfTotal: number = this.props.dataSet.totalValue ? +((matchingData.value / this.props.dataSet.totalValue) * 100).toFixed(1) : 0;
    const parsedValue: string = this.separateThousands(+(matchingData.value).toFixed(0));

    return <div>
      <p className={'header'}>{RegionTranslation.GetRegionTranslation(regionName).english}</p>
      {
        this.props.dataSet.targetRegion ?
          <p>{(this.props.dataSet.dataType === "arrival" ? "Departed" : "Arrived") + " to " + this.props.dataSet.targetRegion.english}: {parsedValue}</p> :
          <p>{parsedValue + (this.props.dataSet.dataType === "totalOrganizations" ? " organizations" : " €")}</p>
      }
      <p>Percentage of total: {percentageOfTotal}%</p>
    </div>;
  }

  render() {
    return (
      <div
        className={'map-container'}
        onWheel={(event) => this.handleScroll(event)}
      >
        <ComposableMap
          projection="mercator"
          width={200}
          height={200}
          style={{
            width: "100%",
            height: "100%"
          }}
        >

          <ZoomableGroup center={[25.5,65.6]} zoom={this.state.mapZoom}>
            <Geographies
              geography={ this.props.geoJson.features }
              disableOptimization
            >
              {(geographies: any, projection: any) => geographies.map((geography: any, index: number) => (
                <Geography
                  key={ index }
                  data-tip={ geography.properties.NAMEFIN }
                  geography={ geography }
                  projection={ projection }
                  cacheId={ geography.properties.GML_ID }
                  style={ this.getGeographyStyle(geography) }
                />
              ))}
            </Geographies>

            {
              this.props.displayDataType === "lines" ?
                <Lines>
                  { this.props.dataSet && this.state.allMarkers ?
                    this.props.dataSet.data.map((data: ParsedData) => {
                      return (
                        <Line
                          key={this.props.dataSet.targetRegion.english + "-" + data.region.english + '_line'}
                          line={{
                            coordinates: {
                              start: this.getMarkerCoordinates(data.region.finnish),
                              end: this.getMarkerCoordinates(this.props.dataSet.targetRegion.finnish)
                            }
                          }}
                          style={{
                            default: { strokeWidth: this.maxMarkerSize * (data.value / this.props.dataSet.maxValue) }
                          }}
                          className={'city-line'}
                          preserveMarkerAspect={false}
                        />
                      );
                    }) : null
                  }
                </Lines> : null
            }

            {
              this.props.displayDataType === "lines" ?
                <Markers>
                  {
                    this.state.allMarkers ? this.state.allMarkers.map((marker) => {
                      return (
                        <Marker
                          ref={marker.name}
                          key={marker.name}
                          marker={marker}
                        >
                          <circle
                            className={'city-marker'}
                            cx={0}
                            cy={0}
                            r={this.state.mapZoom}
                          />
                          {
                            this.state.currentMarker && this.state.currentMarker.name === marker.name ?
                              <text
                                textAnchor="middle"
                                y={marker.markerOffset}
                                className={'city-name'}
                              >
                                {marker.name}
                              </text> : null
                          }
                        </Marker>
                      );
                    }) : null
                  }
                </Markers> : null
            }

          </ZoomableGroup>
        </ComposableMap>

        <div className={'zoom-buttons'}>
          <button
            onClick={() => this.setZoom(-1)}
          >
            <span className={'glyphicon glyphicon-zoom-out'} />
          </button>

          <button
            onClick={() => this.setZoom(1)}
          >
            <span className={'glyphicon glyphicon-zoom-in'} />
          </button>
        </div>

        <ReactTooltip className='regionTooltip' getContent={(regionName) => this.getTooltipContent(regionName)}/>
      </div>
    )
  }
}
