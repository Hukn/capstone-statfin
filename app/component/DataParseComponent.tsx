import * as React from "react";
import { Component } from "react";
const _ = require("lodash");

import '../style.css'

import {AppProps} from "../data-model/props.interface";
import {RegionTranslation} from "../data-model/region-translation.class";

export interface ParsedDataSet {
  maxValue: number;
  totalValue: number;
  dataType: string;
  filters?: string[];
  year: string;
  targetRegion?: RegionTranslation; // If using migration data
  data: ParsedData[]
}

export interface ParsedData {
  value: number;
  region: RegionTranslation;
}

export interface RawGDPData {
  region: string;
  totalPaidTaxes: number;
  taxReturns: number;
  backPaidTaxes: number;
  totalOrganizations: number;
}

export class DataParseComponent extends Component<any, any> {
  public GDPDataTypes: string[] = ['totalPaidTaxes', 'taxReturns', 'backPaidTaxes', 'totalOrganizations'];
  public migrationDataTypes: string[] = ['arrival', 'departure'];

  constructor(props) {
    super(props);
  }

  componentDidUpdate(prevProps) {
    if (!this.props.rawData || !this.props.dataType || !this.props.selectedYear) {
      // No data to parse
      return;
    }

    if (prevProps.rawData && this.props.dataType === prevProps.dataType && this.props.selectedYear === prevProps.selectedYear && _.isEqual(this.props.rawData, prevProps.rawData)) {
      // Same data as previous
      return;
    }

    if (this.GDPDataTypes.indexOf(this.props.dataType) !== -1) {
      // Parse data from GDP dataSet
      this.parseGDPData(this.props.rawData, this.props.dataType, this.props.selectedYear);
      return;
    }

    if (this.migrationDataTypes.indexOf(this.props.dataType) !== -1) {
      // Parse data from Migration dataSet
      this.parseMigrationData(this.props.rawData, this.props.dataType, this.props.selectedYear);
      return;
    }

    console.log("Invalid props for parsing data", this.props);
  }

  private parseMigrationData(dataSet: any, dataType: string, selectedYear: string) {
    const yearIndex: number = dataSet.columns.findIndex(column => column.text === "Year");
    const arrivalIndex: number = dataSet.columns.findIndex(column => column.text === "Region of arrival");
    const departureIndex: number = dataSet.columns.findIndex(column => column.text === "Region of departure");

    const parsedDataSet: ParsedDataSet = {
      maxValue: 0,
      totalValue: 0,
      dataType: dataType,
      filters: null, // Filtering done already during API request
      targetRegion: RegionTranslation.GetRegionTranslation(dataSet.data[0].key[dataType === 'arrival' ? arrivalIndex : departureIndex], 'code'),
      data: [],
      year: selectedYear
    };

    for (const data of dataSet.data) {
      if (selectedYear && selectedYear !== data.key[yearIndex]) {
        continue;
      }

      const parsedData: ParsedData = {
        value: parseFloat(data.values[0]), // Always the first index in migration data
        region: RegionTranslation.GetRegionTranslation(data.key[dataType === 'arrival' ? departureIndex : arrivalIndex], 'code')
      };

      parsedDataSet.totalValue += parsedData.value;
      parsedDataSet.maxValue = parsedData.value > parsedDataSet.maxValue ? parsedData.value : parsedDataSet.maxValue;

      parsedDataSet.data.push(parsedData);
    }

    this.parseComplete(parsedDataSet);
  }

  public parseGDPData(dataSet: any, dataType: string, selectedYear: string) {
    const parsedDataSet: ParsedDataSet = {
      maxValue: 0,
      totalValue: 0,
      dataType: dataType,
      filters: null,
      data: [],
      year: selectedYear
    };

    Object.keys(dataSet).forEach((key: string) => {
      const keyValueArray: RawGDPData[] = dataSet[key];

      for (const keyValue of keyValueArray) {
        if (selectedYear && selectedYear !== key) {
          continue;
        }

        const duplicateRegionData: ParsedData = parsedDataSet.data.find(data => {
          return data.region.english === RegionTranslation.GetRegionTranslation(keyValue.region).english;
        });

        // Add to existing
        if (duplicateRegionData) {
          duplicateRegionData.value += parseFloat(keyValue[dataType]);
        }

        const parsedData: ParsedData = duplicateRegionData ? duplicateRegionData : {
          value: parseFloat(keyValue[dataType]),
          region: RegionTranslation.GetRegionTranslation(keyValue.region)
        };

        parsedDataSet.totalValue += parseFloat(keyValue[dataType]);
        parsedDataSet.maxValue = parsedData.value > parsedDataSet.maxValue ? parsedData.value : parsedDataSet.maxValue;

        if (!duplicateRegionData) {
          parsedDataSet.data.push(parsedData);
        }
      }
    });

    this.parseComplete(parsedDataSet);
  }

  public parseComplete(parsedDataSet: ParsedDataSet): void {
    this.props.onParseComplete(parsedDataSet);
  }

  render() {
    return null
  }
}

export default DataParseComponent;
