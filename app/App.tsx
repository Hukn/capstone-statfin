import * as React from "react";
import * as ReactDOM from "react-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import DataSetComponent from "./component/DataSetComponent";

ReactDOM.render(
  <DataSetComponent compiler="TypeScript" framework="React" />,
  document.getElementById('root')
);
