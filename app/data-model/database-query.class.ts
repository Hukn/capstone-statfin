export class DatabaseQuery {
  constructor(
    public databaseName: string,       // ie. StatFin
    public databaseRealm: string,      // ie. vrm
    public databaseSubRealm: string,   // ie. kuol

    // You can omit everything before the table name in the query
    public tableName: string,          // ie. statfin_kuol_pxt_010.px
  ) { }

  public static NewQuery(databaseName?: string, databaseRealm?: string, databaseSubRealm?: string, tableName?: string) {
    return new this (databaseName, databaseRealm, databaseSubRealm, tableName);
  }
}