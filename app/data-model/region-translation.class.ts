export class RegionTranslation {
  constructor (
    public code: string,
    public finnish: string,
    public english: string,
    public swedish: string,
  ) {}

  public static regionTranslations: {
    code: string,
    finnish: string,
    english: string,
    swedish: string
  }[] = [
    {code: "MK19", finnish: "Lappi", english: "Lapland", swedish: "Lappland"},
    {code: "MK17", finnish: "Pohjois-Pohjanmaa", english: "North Ostrobothnia", swedish: "Norra Österbotten"},
    {code: "MK18", finnish: "Kainuu", english: "Kainuu", swedish: "Kajanaland"},
    {code: "MK12", finnish: "Pohjois-Karjala", english: "North Karelia", swedish: "Norra Karelen"},
    {code: "MK11", finnish: "Pohjois-Savo", english: "Pohjois-Savo", swedish: "Norra Savolax"},
    {code: "MK10", finnish: "Etelä-Savo", english: "Etelä-Savo", swedish: "Södra Savolax"},
    {code: "MK14", finnish: "Etelä-Pohjanmaa", english: "South Ostrobothnia", swedish: "Södra Österbotten"},
    {code: "MK16", finnish: "Keski-Pohjanmaa", english: "Central Ostrobothnia", swedish: "Mellersta Österbotten"},
    {code: "MK15", finnish: "Pohjanmaa", english: "Ostrobothnia", swedish: "Österbotten"},
    {code: "MK06", finnish: "Pirkanmaa", english: "Pirkanmaa", swedish: "Birkaland"},
    {code: "MK13", finnish: "Keski-Suomi", english: "Central Finland", swedish: "Mellersta Finland"},
    {code: "MK04", finnish: "Satakunta", english: "Satakunta", swedish: "Satakunta"},
    {code: "MK02", finnish: "Varsinais-Suomi", english: "Varsinais-Suomi", swedish: "Egentliga Finland"},
    {code: "MK09", finnish: "Etelä-Karjala", english: "South Karelia", swedish: "Södra Karelen"},
    {code: "MK07", finnish: "Päijät-Häme", english: "Päijät-Häme", swedish: "Päijänne-Tavastland"},
    {code: "MK05", finnish: "Kanta-Häme", english: "Kanta-Häme", swedish: "Egentliga Tavastland"},
    {code: "MK01", finnish: "Uusimaa", english: "Helsinki-Uusimaa Region", swedish: "Nyland"},
    {code: "MK08", finnish: "Kymenlaakso", english: "Kymenlaakso", swedish: "Kymmenedalen"},
    {code: "MK21", finnish: "Ahvenanmaa", english: "Åland", swedish: "Åland"}
  ];

  public static availableTranslations: string[] = [
    'code',
    'english',
    'finnish',
    'swedish'
  ];

  public static GetRegionTranslation(region: string, identifier?: string) {
    if (!region) {
      return;
    }

    if (identifier) {
      // Language is known
      return this.regionTranslations.find((translation) => {
        return region === translation[identifier];
      });
    }

    // Check all available languages instead
    return this.regionTranslations.find((translation) => {
      for (const availableTranslation of this.availableTranslations) {
        if (region === translation[availableTranslation]) {
          return true;
        }
      }

      return;
    });
  }
}