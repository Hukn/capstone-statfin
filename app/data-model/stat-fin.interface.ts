export interface StatFinResponse {
  config: any;
  data: any;
  header: any;
  request: any;

  status: number;
  statusText: string;
}

export interface StatFinConfig {
  CORS: boolean;
  maxCalls: number;
  maxValues: number;
  timeWindow: number;
}

export interface DatabaseInfo {
  dbid: string;
  text: string;
}

export interface DatabaseRealm {
  id: string;
  text: string;
  type: string; // 1 = sub-level, t = table
  updated?: string;
}

export interface DataSet {
  id: string;
  text: string;
  type: string; // 1 = sub-level, t = table
  updated?: string;
}

export interface TableMetadata {
  code: string; // Name for query
  text: string; // Name in requested language

  values: string[]; // Values with descriptions
  valueTexts: string[]; // Values for query

  elimination: boolean;
}

export interface DataSetQuery {
  query: any[];
  response: {
    // File format: px, xlsx, csv, json or json-stat
    format: string;
  }
}

export interface StatFinState {
  statFinConfig: StatFinConfig;
  statFinPopulation: any;
  statFinStructure: any;
  databaseList: DatabaseInfo[]
}

export interface StatFinDataSet {
  columns: {
    code: string,
    text: string,
    type: string
  }[];
  comments: any[]; // Unknown
  data: {
    key: string[],
    values: string[]
  }[];
}

export interface ParsedDataSet {
  year: string;
  regionOfArrival: string;
  regionOfDeparture: string;
  value: number;
  maxValue: number;
}